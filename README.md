# Media Library Theme Reset

## Table of contents

- Introduction
- Requirements
- Installation
- Known issues
- Fixing theme specific issues
- Maintainers

## Introduction

The core Media Library is designed for use in the admin theme. Sites that edit content or layouts in a
front-end theme can experience a broken media library layout.

The Media Library is rendered in the front end theme when:
- Using Layout Builder.
- Editing content, if "Use the administration theme when editing or creating content" is disabled in the Appearance settings.
- A user is not allowed to use an admin theme.
- A front end theme is selected as an admin theme.
- User facing web-forms that use the Media system.

This module adds theming for the core Media Library when it is displayed in a front end theme.

This module simulates an administrative look and feel for the Media Library within the front-end theme.

Rather than requiring themes to add additional templates and CSS to display the Media Library correctly,
this module attaches Claro's CSS whenever the Media Library is loaded on a page.

This module provides some templates copied from Claro. They can be overridden and customized by copying
them to your theme's templates directory.

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see "Installing Drupal Modules":

- https://www.drupal.org/docs/extending-drupal/installing-drupal-modules

## Known issues
Any themes based on Stable 9 will have templates that are missing essential CSS classes.
This is because Stable 9 provides a copy of the upstream media library module's template which automatically
overrides the template provided by this module.
This will cause broken layouts if your theme does not override these templates.

To fix this copy the following templates from this module into your theme's templates directory.
- media--media-library.html.twig
- media-library-wrapper.html.twig

## Fixing theme specific issues
The module provides only the styles needed for media library to function and look reasonably correct.
Some themes will need fixes in order to display correctly. Ideally theme problems should be fixed in the theme.

If the theme is correct but still has display issues then a theme specific override can be created.

Theme specific overrides are done by creating a css file themename-fixes.css and adding an entry in
media_library_theme_reset_form_media_library_add_form_alter() to load the fixes file for that theme only.

## Maintainers
<!--- cspell:disable -->
- Ryan Hovland - [rhovland](https://www.drupal.org/u/rhovland)
- Mark Fullmer - [mark_fullmer](https://www.drupal.org/u/mark_fullmer)
<!--- cspell:enable -->

For a full description of the module, visit the project page:

- https://www.drupal.org/project/media_library_theme_reset

Submit bug reports and feature suggestions, or track changes in the issue queue:

- https://www.drupal.org/project/issues/media_library_theme_reset
