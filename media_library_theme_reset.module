<?php

/**
 * @file
 * Contains media_library_theme_reset.module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Form\ViewsForm;
use Drupal\views\ViewExecutable;

/**
 * Implements hook_theme().
 *
 * Register the media library templates.
 * If your active theme contains these templates then they will be used instead.
 */
function media_library_theme_reset_theme() {
  return [
    'container__media_library_content' => [
      'base hook' => 'container',
    ],
    'container__media_library_widget_selection' => [
      'base hook' => 'container',
    ],
    'details__media_library_add_form_selected_media' => [
      'base hook' => 'details',
    ],
    'fieldset__media_library_widget' => [
      'base hook' => 'fieldset',
    ],
    'item_list__media_library_add_form_media_list' => [
      'base hook' => 'item_list',
    ],
    'links__media_library_menu' => [
      'base hook' => 'links',
    ],
    // This one is already defined in media_library module.
    // It appears to override that template.
    // However there is no way to know if this will always override that module.
    'media__media_library' => [
      'base hook' => 'media',
    ],
    'media_library_item__small' => [
      'base hook' => 'media_library_item',
    ],
    'media_library_item' => [
      'base hook' => 'media_library_item',
    ],
    // This one is already defined in media library. Might need to do more to
    // ensure this one is selected over the original.
    'media_library_wrapper' => [
      'base hook' => 'media_library_wrapper',
    ],
    'views_exposed_form__media_library' => [
      'base hook' => 'views_exposed_form',
    ],
    'views_view__media_library' => [
      'base hook' => 'views_view__media_library',
    ],
    'views_view_unformatted__media_library' => [
      'base hook' => 'views_view_unformatted',
    ],
  ];
}

/**
 * Implements hook_form_alter().
 */
function media_library_theme_reset_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // Copied from claro.theme claro_form_alter().
  $form_object = $form_state->getFormObject();
  if ($form_object instanceof ViewsForm && strpos($form_object->getBaseFormId(), 'views_form_media_library') === 0) {
    if (isset($form['header'])) {
      $form['header']['#attributes']['class'][] = 'media-library-views-form__header';
      $form['header']['media_bulk_form']['#attributes']['class'][] = 'media-library-views-form__bulk_form';
    }
    $form['actions']['submit']['#attributes']['class'] = ['media-library-select'];
    $form['#attributes']['class'][] = 'media-library-views-form';
  }

  // Add CSS classes to the views exposed form actions.
  if ($form_id === 'views_exposed_form' && strpos($form['#id'], 'views-exposed-form-media-library-widget') === 0) {
    $form['actions']['#attributes']['class'][] = 'media-library-view--form-actions';
    // Ensure the actions button has the same margin and padding as
    // the rest of the form items.
    $form['actions']['#attributes']['class'][] = 'form-item';
  }
}

/**
 * Implements hook_preprocess_links__media_library_menu().
 *
 * This targets the menu of available media types in the media library's modal
 * dialog.
 *
 * @todo Do this in the relevant template once
 *   https://www.drupal.org/project/drupal/issues/3088856 is resolved.
 *
 * Copied from claro.theme.
 */
function media_library_theme_reset_preprocess_links__media_library_menu(array &$variables) {
  foreach ($variables['links'] as &$link) {
    // Add a class to the Media Library menu items.
    $link['attributes']->addClass('media-library-menu__item');
    $link['link']['#options']['attributes']['class'][] = 'media-library-menu__link';
  }
}

/**
 * Implements hook_preprocess_views_view_fields().
 *
 * This targets each rendered media item in the grid display of the media
 * library's modal dialog.
 *
 * Copied from claro.theme.
 */
function media_library_theme_reset_preprocess_views_view_fields__media_library(array &$variables) {
  // Add classes to media rendered entity field so it can be targeted for
  // styling. Adding this class in a template is very difficult to do.
  if (isset($variables['fields']['rendered_entity']->wrapper_attributes)) {
    $variables['fields']['rendered_entity']->wrapper_attributes->addClass('media-library-item__click-to-select-trigger');
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 *
 * Copied from claro.theme.
 */
function media_library_theme_reset_form_media_library_add_form_alter(array &$form, FormStateInterface $form_state) {
  $form['#attributes']['class'][] = 'media-library-add-form';

  // Attach the media library CSS.
  $form['#attached']['library'][] = 'claro/media_library.theme';
  $form['#attached']['library'][] = 'media_library_theme_reset/media_library';

  // Attach theme specific fixes CSS.
  $theme_chain = media_library_theme_reset_get_active_theme_chain();
  if (in_array('olivero', $theme_chain, TRUE)) {
    $form['#attached']['library'][] = 'media_library_theme_reset/olivero_fixes';
  }

  // If there are unsaved media items, apply styling classes to various parts
  // of the form.
  if (isset($form['media'])) {
    $form['#attributes']['class'][] = 'media-library-add-form--with-input';

    // Put a wrapper around the informational message above the unsaved media
    // items.
    $form['description']['#template'] = '<p class="media-library-add-form__description">{{ text }}</p>';
  }
  else {
    $form['#attributes']['class'][] = 'media-library-add-form--without-input';
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Copied from claro.theme.
 */
function media_library_theme_reset_form_media_library_add_form_upload_alter(array &$form, FormStateInterface $form_state) {
  $form['#attributes']['class'][] = 'media-library-add-form--upload';
  if (isset($form['container']['upload'])) {
    // Set this flag so we can prevent the details element from being added
    // in \Drupal\claro\ClaroPreRender::managedFile.
    $form['container']['upload']['#do_not_wrap_in_details'] = TRUE;
  }
  if (isset($form['container'])) {
    $form['container']['#attributes']['class'][] = 'media-library-add-form__input-wrapper';
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Copied from claro.theme.
 */
function media_library_theme_reset_form_media_library_add_form_oembed_alter(array &$form, FormStateInterface $form_state) {
  $form['#attributes']['class'][] = 'media-library-add-form--oembed';

  // If no media items have been added yet, add a couple of styling classes
  // to the initial URL form.
  if (isset($form['container'])) {
    $form['container']['#attributes']['class'][] = 'media-library-add-form__input-wrapper';
    $form['container']['url']['#attributes']['class'][] = 'media-library-add-form-oembed-url';
    $form['container']['submit']['#attributes']['class'][] = 'media-library-add-form-oembed-submit';
  }
}

/**
 * Implements hook_preprocess_item_list__media_library_add_form_media_list().
 *
 * This targets each new, unsaved media item added to the media library, before
 * they are saved.
 *
 * Copied from claro.theme.
 */
function media_library_theme_reset_preprocess_item_list__media_library_add_form_media_list(array &$variables) {
  foreach ($variables['items'] as &$item) {
    $item['value']['preview']['#attributes']['class'][] = 'media-library-add-form__preview';
    $item['value']['fields']['#attributes']['class'][] = 'media-library-add-form__fields';
    $item['value']['remove_button']['#attributes']['class'][] = 'media-library-add-form__remove-button';

    $item['value']['remove_button']['#attributes']['class'][] = 'button--extrasmall';
    // #source_field_name is set by AddFormBase::buildEntityFormElement()
    // to help themes and form_alter hooks identify the source field.
    $fields = &$item['value']['fields'];
    $source_field_name = $fields['#source_field_name'];

    // Set this flag so we can remove the details element.
    $fields[$source_field_name]['widget'][0]['#do_not_wrap_in_details'] = TRUE;

    if (isset($fields[$source_field_name])) {
      $fields[$source_field_name]['#attributes']['class'][] = 'media-library-add-form__source-field';
    }
  }
}

/**
 * Implements hook_preprocess_media_library_item__widget().
 *
 * This targets each media item selected in an entity reference field.
 *
 * Copied from claro.theme.
 */
function media_library_theme_reset_preprocess_media_library_item__widget(array &$variables) {
  $variables['content']['remove_button']['#attributes']['class'][] = 'media-library-item__remove';
  $variables['content']['remove_button']['#attributes']['class'][] = 'icon-link';
}

/**
 * Implements hook_preprocess_media_library_item__small().
 *
 * This targets each pre-selected media item selected when adding new media in
 * the modal media library dialog.
 *
 * Copied from claro.theme.
 */
function media_library_theme_reset_preprocess_media_library_item__small(array &$variables) {
  $variables['content']['select']['#attributes']['class'][] = 'media-library-item__click-to-select-checkbox';
}

/**
 * Implements hook_views_pre_render().
 *
 * Copied from claro.theme.
 */
function media_library_theme_reset_views_pre_render(ViewExecutable $view) {
  $add_classes = function (&$option, array $classes_to_add) {
    $classes = preg_split('/\s+/', $option);
    $classes = array_filter($classes);
    $classes = array_merge($classes, $classes_to_add);
    $option = implode(' ', array_unique($classes));
  };

  if ($view->id() === 'media_library') {
    if ($view->display_handler->options['defaults']['css_class']) {
      $add_classes($view->displayHandlers->get('default')->options['css_class'], ['media-library-view']);
    }
    else {
      $add_classes($view->display_handler->options['css_class'], ['media-library-view']);
    }

    if ($view->current_display === 'page') {
      if (array_key_exists('media_bulk_form', $view->field)) {
        $add_classes($view->field['media_bulk_form']->options['element_class'], ['media-library-item__click-to-select-checkbox']);
      }
      if (array_key_exists('rendered_entity', $view->field)) {
        $add_classes($view->field['rendered_entity']->options['element_class'], ['media-library-item__content']);
      }
      if (array_key_exists('edit_media', $view->field)) {
        $add_classes($view->field['edit_media']->options['alter']['link_class'], ['media-library-item__edit']);
        $add_classes($view->field['edit_media']->options['alter']['link_class'], ['icon-link']);
      }
      if (array_key_exists('delete_media', $view->field)) {
        $add_classes($view->field['delete_media']->options['alter']['link_class'], ['media-library-item__remove']);
        $add_classes($view->field['delete_media']->options['alter']['link_class'], ['icon-link']);
      }
    }
    elseif (str_starts_with($view->current_display, 'widget')) {
      if (array_key_exists('rendered_entity', $view->field)) {
        $add_classes($view->field['rendered_entity']->options['element_class'], ['media-library-item__content']);
      }
      if (array_key_exists('media_library_select_form', $view->field)) {
        $add_classes($view->field['media_library_select_form']->options['element_wrapper_class'], ['media-library-item__click-to-select-checkbox']);
      }

      if ($view->display_handler->options['defaults']['css_class']) {
        $add_classes($view->displayHandlers->get('default')->options['css_class'], ['media-library-view--widget']);
      }
      else {
        $add_classes($view->display_handler->options['css_class'], ['media-library-view--widget']);
      }
    }
  }
}

/**
 * Gets a list of the active theme and it's parents.
 *
 * @return array
 *   The full chain of themes the active theme is using.
 */
function media_library_theme_reset_get_active_theme_chain() {
  $active_theme = \Drupal::service('theme.manager')->getActiveTheme();
  $theme_chain[] = $active_theme->getName();
  $base_themes = $active_theme->getBaseThemeExtensions();
  foreach ($base_themes as $base_theme) {
    $theme_chain[] = $base_theme->getName();
  }

  return $theme_chain;
}
